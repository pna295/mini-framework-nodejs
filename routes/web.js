var express = require('express');
var router = express.Router(); 

//Imports des controllers
var usersCtrl = require('../controllers/usersController');




//Définition des routes
router.route('/users/index').get(usersCtrl.index)
router.route('/users/register').post(usersCtrl.register);
router.route('/users/login').post(usersCtrl.login);

// export des routes
module.exports = router;







 //Router
/*exports.router(function () {
    var webRouter = express.Router();

    //Definitions des différentes routes
    webRouter.route('/users/register').post(usersCtrl.register);
    webRouter.route('/users/login').post(usersCtrl.login);

    return webRouter;


})();*/