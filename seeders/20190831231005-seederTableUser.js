'use strict';
var bcrypt = require('bcrypt');
var password = 'levb'

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
    */
      
      return queryInterface.bulkInsert('users', [{
        
        pseudo: 'vb',
        password: bcrypt.hashSync(password,5),
        createdAt: new Date(),
        updatedAt: new Date()
        //isBetaMember: false
      }], {});
    
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
*/
    //  Example:
      return queryInterface.bulkDelete('users', null, {});
    
  }
};
